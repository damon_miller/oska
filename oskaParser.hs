

--Oska Board Parsing Program
--Written By Stewart Grant
--Modified Nov 2 2013

--Overview 
-- THe oska parser program is desigend to map a given oska board input into a workable marix
-- where the oska board is of the form

-- ["1234"
-- ,"567"
-- ,"89"
-- ,"abc"
-- ,"defg"]

-- translates to the corresponding output matrix

-- xxx4xx
-- xx37xx
-- x269cg
-- 158bfx
-- xxaexx
-- xxdxxx

module OskaParser where

import Data.List -- transpose
--Main Calling Function takes the given oska lists and retruns a workable matrix with correctly mapped values
parseOskaBoard :: [String] -> [String]
parseOskaBoard inputLists =  populateMatrix inputLists (makeMatrix inputLists) 0 0

makeMatrix :: [String] -> [String]
makeMatrix input = genBoard (2*(length (input!!0)) -2) (2*(length (input!!0)) -2)

genBoard :: Int -> Int -> [String]
genBoard size index
	| index == 0		= []
	| otherwise			= (xLine size) : (genBoard size (index - 1))

xLine :: Int -> String
xLine index
	| index == 0		=[]
	| otherwise			= 'X': (xLine (index -1)) 

populateMatrix :: [String] -> [String] -> Int -> Int-> [String]
populateMatrix input matrix listNumber overallOskaIndex
	| listNumber == (length input)			= matrix
	| otherwise								= populateMatrix input (addLine matrix (input !! listNumber) (length (input !!0)) 0 listNumber overallOskaIndex) (listNumber+1) (overallOskaIndex + (length (input !! listNumber)))

-- add line takes a line of input from the oska input and adds every element to the output matrix
-- matrix 			- the working matrix, use for future manipulation
-- line 			- a particular line from the oska input set of lists
-- numPlayers 		- the number of players per team at the beginning of the game
-- indexOfElement 	- the position of the element currenly being added to the matrix, with respect to the oska input
-- listNumber		- the line number corresponding to the oska input, which is being added 
addLine :: [String] -> String -> Int -> Int -> Int -> Int ->[String]
addLine matrix line numPlayers indexofLineElement listNumber overallOskaIndex
	| indexofLineElement == (length line)  												= matrix
	| lessThenHalf 																		= addLine appendLTHalf line numPlayers (indexofLineElement + 1) listNumber (overallOskaIndex +1)
	| otherwise																			= addLine appendGTHalf line numPlayers (indexofLineElement + 1) listNumber (overallOskaIndex +1)

	where 	addLessThenHalfElement  = (addElement (matrix !! (numPlayers - indexofLineElement -1)) 	(line !! indexofLineElement) numPlayers indexofLineElement listNumber (length matrix) 0 overallOskaIndex);
 			addMoreThenHalfElement  = (addElement (matrix !! (listNumber - indexofLineElement + 1)) (line !! indexofLineElement) numPlayers indexofLineElement listNumber (length matrix) 0 overallOskaIndex);
 			appendGTHalf			= (appendLine matrix addMoreThenHalfElement (listNumber - indexofLineElement + 1) 0);
 			appendLTHalf			= (appendLine matrix addLessThenHalfElement (numPlayers - indexofLineElement -1) 0);
 			lessThenHalf 			= (overallOskaIndex <  ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1));


--Append Line takes a 2 dimentional matrix and a line to replace at a certian index, it returns the matrix with that line replaced
appendLine :: [String] -> String -> Int -> Int -> [String]
appendLine matrix matrixLine linenumber count
	| count == linenumber		= matrixLine : (tail matrix)
	| otherwise					= (head matrix) : (appendLine (tail matrix) matrixLine linenumber (count+1))	


-- addElement places a specific character into a matrix list based on its position in the oska arrays
-- matrixLine 		- the line of the working matrix which is having an element added
-- character		- the character being added to the matrix array
-- numPlayers 		- the number of players each team starts with
-- listPosition 	- the position of the element with respect to its specific oska input line
-- listNumber		- the number of the list in the oska input
-- lineLength		- the length of a workable matrix line
-- overallOskaIndex - the overall Position of the element being added relitive to the lengths of all the input strings appended 
addElement :: String -> Char -> Int -> Int -> Int -> Int -> Int-> Int -> String
addElement matrixLine character numPlayers listPosition listNumber lineLength count overallOskaIndex
	| (null matrixLine)																	= []
	| lessThenHalf && (count == (listPosition + listNumber)) 							= character : (tail matrixLine) 
	| moreThenHalf && (count == (lineLength - numPlayers + listPosition ))			= character : (tail matrixLine)
	| otherwise																			= (head matrixLine) : (addElement (tail matrixLine) character numPlayers listPosition listNumber lineLength (count+1) overallOskaIndex)
	
	where 	lessThenHalf = (overallOskaIndex <  ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1));
			moreThenHalf = (overallOskaIndex >= ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1))




--oskaMatrixAlphabet = [
--	"XXXdXX", 
--	"XXcgXX", 
--	"Xbfilp", 
--	"aehkoX", 
--	"XXjnXX", 
--	"XXmXXX"]
--
--pt = parseMatrix oskaMatrixAlphabet 4

parseMatrix :: [String] -> Int -> [String]
parseMatrix matrix numplayers = (getHalfLines matrix numplayers 0 0) ++ (tail (reverse(getHalfLines (reverse(transpose (reverse matrix))) numplayers 0 0)))

getHalfLines :: [String] -> Int -> Int -> Int -> [String]
getHalfLines matrix numPlayers overallOskaIndex lineNumber
	| overallOskaIndex == (numPlayers*((numPlayers) + 1)) 	= []
	| lessThenHalf 											= (parseLine (transpose matrix) numPlayers 0) : (getHalfLines (transpose(removeLine (transpose matrix) numPlayers 0)) numPlayers (overallOskaIndex + (numPlayers - lineNumber)) (lineNumber +1))
	| otherwise												= []
	where 	lessThenHalf = (overallOskaIndex <  ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1))


parseLine :: [String] -> Int -> Int-> String
parseLine matrix numPlayers count
	| numPlayers == count									= []
	| (doesLineHaveValues (matrix !! count)) /= True		= parseLine matrix numPlayers (count+1)
	| otherwise												= (getFirstElement (matrix !! count)) : (parseLine matrix numPlayers (count+1))

removeLine :: [String] -> Int -> Int -> [String]
removeLine matrix numPlayers count
	| numPlayers == count								= matrix
	| (doesLineHaveValues (matrix !! count))/= True		= removeLine matrix numPlayers (count +1)
	| otherwise											= (removeLine (replaceLine matrix (removeFirstElement (matrix !! count)) count 0) numPlayers (count +1))

replaceLine :: [String] -> String -> Int -> Int ->[String]
replaceLine matrix line index counter
	| index == counter		= line : (tail matrix)
	| otherwise				= (head matrix) : (replaceLine (tail matrix) line index (counter +1))

removeFirstElement :: String -> String
removeFirstElement line
	| null line 			= line
	| (head line) == 'X' 	= (head line) : (removeFirstElement (tail line))
	| otherwise				= ('X' : (tail line))

getFirstElement :: String -> Char
getFirstElement line
	| null line 			= '-'
	| (head line) == 'X' 	= (getFirstElement (tail line))
	| otherwise				= (head line)

doesLineHaveValues :: String -> Bool
doesLineHaveValues line
	| null line 			= False
	| (head line) /= 'X'	= True
	| otherwise				= doesLineHaveValues (tail line)


--printStrList :: [String] -> IO ()
--printStrList [] = putStrLn ""
--printStrList (x:xs) = do
 --   putStrLn x
 --   printStrList xs


testBoard = ["1234","567","89","abc","defg"]
testBoard2 = ["wwww","---","--","---","bbbb"]
testBoard3 = ["wwwww","----","---","--" ,"---","----","bbbbb"]





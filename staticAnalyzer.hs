module StaticAnalyzer  where

import OskaParser -- (parseOskaBoard),(parseMatrix)
import Data.List -- transpose

boardScoreWhite :: [String] -> Int
boardScoreWhite board = boardScore board 'w'

boardScoreBlack :: [String] -> Int
boardScoreBlack board = boardScore board 'b'

boardScore :: [String] -> Char -> Int
boardScore board team = countX*(playerCountBonus board team) + adX*(attackDefenceBonus board team) + gsX*(goalStateBonus board team) + (positionBonus board team)

---Count Opponents determins the bonus the state should get by the number of players on the board
playerCountBonus :: [String] -> Char -> Int
playerCountBonus board team
	| team == 'w' 		= (sumPlayers board 'w') - (sumPlayers board 'b')
	| team == 'b'		= (sumPlayers board 'b') - (sumPlayers board 'w')
	| otherwise			= 0

sumPlayers :: [String] -> Char -> Int
sumPlayers board team
	| null board 				= 0
	| null (head board)			= sumPlayers (tail board) team
	| head (head board) == team = (1 + (sumPlayers ((tail (head board)):(tail board)) team))
	| otherwise					= (sumPlayers ((tail (head board)):(tail board)) team)

-- attack defence bonus rates your currnt attacking and defending position as the value of the number of succsusfful atacks you can play - your oponents succussfull attacks

attackDefenceBonus :: [String] -> Char -> Int
attackDefenceBonus board team
	| team == 'w'		= (2*(whiteAttacks board) - (blackAttacks board))
	| team == 'b'		= (2*(blackAttacks board) - (whiteAttacks board))

-- each string denotes a favorable attacking position for a given team, in each of these situations white has the ability to attack, while being defended
-- these states are not given too much value as to supercede actually taking a piece, hower they are good configurations to aim for while trying to win
whiteAttack :: String -> Bool
whiteAttack attackPattern
	| attackPattern == "wwb-" 	= True
	| attackPattern == "-bww" 	= True
	| attackPattern == "-bwX" 	= True
	| attackPattern == "Xwb-" 	= True
	| otherwise 				= False

blackAttack :: String -> Bool
blackAttack attackPattern
	| attackPattern == "bbw-" 	= True
	| attackPattern == "-wbb" 	= True
	| attackPattern == "-wbX" 	= True
	| attackPattern == "Xbw-" 	= True
	| otherwise 				= False

whiteAttacksPerLine :: String -> Int
whiteAttacksPerLine line 
	| null (tail(tail line))														= 0
	| whiteAttack ((head line): (head (tail line)) : (head(tail(tail line))): []) 	= (1 + (whiteAttacksPerLine (tail line)))
	| otherwise																		= (whiteAttacksPerLine (tail line))

blackAttacksPerLine :: String -> Int
blackAttacksPerLine line 
	| null (tail(tail line))														= 0
	| blackAttack ((head line): (head (tail line)) : (head(tail(tail line))): []) 	= (1 + (blackAttacksPerLine (tail line)))
	| otherwise																		= (blackAttacksPerLine (tail line))

blackAttacks :: [String] -> Int
blackAttacks board = ((foldr (+) 0 (map blackAttacksPerLine board)) + (foldr (+) 0 (map blackAttacksPerLine (transpose board))))

whiteAttacks :: [String] -> Int
whiteAttacks board = ((foldr (+) 0 (map whiteAttacksPerLine board)) + (foldr (+) 0 (map whiteAttacksPerLine (transpose board))))

testBoard 	 = ["1234","567","89","abc","defg"]
testBoard2 	= ["w-ww","-w-","--","---","bbb-"]
testBoard3 	= ["wwwww","----","---","--" ,"---","---w","bbbbb"]
attack 		= ["----","-ww","bb","bbb","----"]

-- goal State is awarded if the team has won the game
goalStateBonus :: [String] -> Char -> Int
goalStateBonus board team
	| (team == 'w') && ((sumPlayers board 'b') == 0) 												= 1
	| (team == 'b') && ((sumPlayers board 'w') == 0) 												= 1
	| (team == 'w') && (endZone (last (parseMatrix board numPlayers)) 'w' (sumPlayers board team) 0)= 1
	| (team == 'b') && (endZone (head (parseMatrix board numPlayers)) 'b' (sumPlayers board team) 0)= 1
	| otherwise																						= 0
	where numPlayers = (((length board) `div` 2)+1)

--end zone is really weird because it actually uses the format of the oska board which was passed originally, simply because it is easier to determine the winning state with that model

endZone :: String ->Char ->Int-> Int-> Bool
endZone line team players count
	| (null line) && (count == players) 	= True
	| (null line) && (count /= players) 	= False 
	| (head line) == team					= endZone (tail line) team players (count+1)
	| otherwise								= endZone (tail line) team players count

--positionBonus

-- Position Bonus nneds to be done for both the white and black pieces

positionBonus :: [String] -> Char -> Int
positionBonus matrix team
	| team == 'w' 		= (sumPositionBonus (parseMatrix matrix (((length matrix) `div` 2)+1)) team 0)		--the funny expression is the number of players
	| team == 'b'		= (sumPositionBonus (reverse(parseMatrix matrix (((length matrix) `div` 2)+1))) team 0)

sumPositionBonus :: [String] -> Char -> Int -> Int
sumPositionBonus oskaBoard team count
	| null oskaBoard 				= 0
	| count == (length oskaBoard) 	= 0
	| otherwise 					= (((count+1)*(sumLine (head oskaBoard) team )) + (sumPositionBonus (tail oskaBoard) team (count +1)))

sumLine :: String -> Char -> Int
sumLine line team
	| null line 	      	= 0
	| (head line) == team   = (1 + (sumLine (tail line) team))
	| otherwise				= sumLine (tail line) team  
-------end POsition bonus
o1 = [
	"XXX-XX", 
	"XX--XX", 
	"X---w-", 
	"b----X", 
	"XX-wXX", 
	"XXwXXX"]



countX :: Int 
countX 	= 100
adX ::Int
adX		= 25
gsX :: Int
gsX		= 1000
module V9C9 where
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------Parsing Module----------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Oska Board Parsing Program
--Written By Stewart Grant
--Modified Nov 2 2013

--Overview 
-- THe oska parser program is desigend to map a given oska board input into a workable marix
-- where the oska board is of the form

-- ["1234"
-- ,"567"
-- ,"89"
-- ,"abc"
-- ,"defg"]

-- translates to the corresponding output matrix

-- xxx4xx
-- xx37xx
-- x269cg
-- 158bfx
-- xxaexx
-- xxdxxx


import Data.List -- transpose
--Main Calling Function takes the given oska lists and retruns a workable matrix with correctly mapped values
parseOskaBoard_c6n8 :: [String] -> [String]
parseOskaBoard_c6n8 inputLists =  populateMatrix_c6n8 inputLists (makeMatrix_c6n8 inputLists) 0 0

makeMatrix_c6n8 :: [String] -> [String]
makeMatrix_c6n8 input = genBoard_c6n8 (2*(length (input!!0)) -2) (2*(length (input!!0)) -2)

genBoard_c6n8 :: Int -> Int -> [String]
genBoard_c6n8 size index
	| index == 0		= []
	| otherwise			= (xLine_c6n8 size) : (genBoard_c6n8 size (index - 1))

xLine_c6n8 :: Int -> String
xLine_c6n8 index
	| index == 0		=[]
	| otherwise			= 'X': (xLine_c6n8 (index -1)) 

populateMatrix_c6n8 :: [String] -> [String] -> Int -> Int-> [String]
populateMatrix_c6n8 input matrix listNumber overallOskaIndex
	| listNumber == (length input)			= matrix
	| otherwise								= populateMatrix_c6n8 input (addLine_c6n8 matrix (input !! listNumber) (length (input !!0)) 0 listNumber overallOskaIndex) (listNumber+1) (overallOskaIndex + (length (input !! listNumber)))

-- add line takes a line of input from the oska input and adds every element to the output matrix
-- matrix 			- the working matrix, use for future manipulation
-- line 			- a particular line from the oska input set of lists
-- numPlayers 		- the number of players per team at the beginning of the game
-- indexOfElement 	- the position of the element currenly being added to the matrix, with respect to the oska input
-- listNumber		- the line number corresponding to the oska input, which is being added 
addLine_c6n8 :: [String] -> String -> Int -> Int -> Int -> Int ->[String]
addLine_c6n8 matrix line numPlayers indexofLineElement listNumber overallOskaIndex
	| indexofLineElement == (length line)  												= matrix
	| lessThenHalf 																		= addLine_c6n8 appendLTHalf line numPlayers (indexofLineElement + 1) listNumber (overallOskaIndex +1)
	| otherwise																			= addLine_c6n8 appendGTHalf line numPlayers (indexofLineElement + 1) listNumber (overallOskaIndex +1)

	where 	addLessThenHalfElement  = (addElement_c6n8 (matrix !! (numPlayers - indexofLineElement -1)) 	(line !! indexofLineElement) numPlayers indexofLineElement listNumber (length matrix) 0 overallOskaIndex);
 			addMoreThenHalfElement  = (addElement_c6n8 (matrix !! (listNumber - indexofLineElement + 1)) (line !! indexofLineElement) numPlayers indexofLineElement listNumber (length matrix) 0 overallOskaIndex);
 			appendGTHalf			= (appendLine_c6n8 matrix addMoreThenHalfElement (listNumber - indexofLineElement + 1) 0);
 			appendLTHalf			= (appendLine_c6n8 matrix addLessThenHalfElement (numPlayers - indexofLineElement -1) 0);
 			lessThenHalf 			= (overallOskaIndex <  ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1));


--Append Line takes a 2 dimentional matrix and a line to replace at a certian index, it returns the matrix with that line replaced
appendLine_c6n8 :: [String] -> String -> Int -> Int -> [String]
appendLine_c6n8 matrix matrixLine_c6n8 linenumber count
	| count == linenumber		= matrixLine_c6n8 : (tail matrix)
	| otherwise					= (head matrix) : (appendLine_c6n8 (tail matrix) matrixLine_c6n8 linenumber (count+1))	


-- addElement_c6n8 places a specific character into a matrix list based on its position in the oska arrays
-- matrixLine_c6n8 		- the line of the working matrix which is having an element added
-- character		- the character being added to the matrix array
-- numPlayers 		- the number of players each team starts with
-- listPosition 	- the position of the element with respect to its specific oska input line
-- listNumber		- the number of the list in the oska input
-- lineLength		- the length of a workable matrix line
-- overallOskaIndex - the overall Position of the element being added relitive to the lengths of all the input strings appended 
addElement_c6n8 :: String -> Char -> Int -> Int -> Int -> Int -> Int-> Int -> String
addElement_c6n8 matrixLine_c6n8 character numPlayers listPosition listNumber lineLength count overallOskaIndex
	| (null matrixLine_c6n8)																	= []
	| lessThenHalf && (count == (listPosition + listNumber)) 							= character : (tail matrixLine_c6n8) 
	| moreThenHalf && (count == (lineLength - numPlayers + listPosition ))			= character : (tail matrixLine_c6n8)
	| otherwise																			= (head matrixLine_c6n8) : (addElement_c6n8 (tail matrixLine_c6n8) character numPlayers listPosition listNumber lineLength (count+1) overallOskaIndex)
	
	where 	lessThenHalf = (overallOskaIndex <  ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1));
			moreThenHalf = (overallOskaIndex >= ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1))

--oskaMatrixAlphabet = [
--	"XXXdXX", 
--	"XXcgXX", 
--	"Xbfilp", 
--	"aehkoX", 
--	"XXjnXX", 
--	"XXmXXX"]
--
--pt = parseMatrix_c6n8 oskaMatrixAlphabet 4

parseMatrix_c6n8 :: [String] -> Int -> [String]
parseMatrix_c6n8 matrix numplayers = (getHalfLines_c6n8 matrix numplayers 0 0) ++ (tail (reverse(getHalfLines_c6n8 (reverse(transpose (reverse matrix))) numplayers 0 0)))

getHalfLines_c6n8 :: [String] -> Int -> Int -> Int -> [String]
getHalfLines_c6n8 matrix numPlayers overallOskaIndex lineNumber
	| overallOskaIndex == (numPlayers*((numPlayers) + 1)) 	= []
	| lessThenHalf 											= (parseLine_c6n8 (transpose matrix) numPlayers 0) : (getHalfLines_c6n8 (transpose(removeLine_c6n8 (transpose matrix) numPlayers 0)) numPlayers (overallOskaIndex + (numPlayers - lineNumber)) (lineNumber +1))
	| otherwise												= []
	where 	lessThenHalf = (overallOskaIndex <  ((numPlayers*((numPlayers) + 1) `div` 2)+(numPlayers*((numPlayers) + 1) `mod` 2)-1))


parseLine_c6n8 :: [String] -> Int -> Int-> String
parseLine_c6n8 matrix numPlayers count
	| numPlayers == count									= []
	| (doesLineHaveValues_c6n8 (matrix !! count)) /= True		= parseLine_c6n8 matrix numPlayers (count+1)
	| otherwise												= (getFirstElement_c6n8 (matrix !! count)) : (parseLine_c6n8 matrix numPlayers (count+1))

removeLine_c6n8 :: [String] -> Int -> Int -> [String]
removeLine_c6n8 matrix numPlayers count
	| numPlayers == count								= matrix
	| (doesLineHaveValues_c6n8 (matrix !! count))/= True		= removeLine_c6n8 matrix numPlayers (count +1)
	| otherwise											= (removeLine_c6n8 (replaceLine_c6n8 matrix (removeFirstElement_c6n8 (matrix !! count)) count 0) numPlayers (count +1))

replaceLine_c6n8 :: [String] -> String -> Int -> Int ->[String]
replaceLine_c6n8 matrix line index counter
	| index == counter		= line : (tail matrix)
	| otherwise				= (head matrix) : (replaceLine_c6n8 (tail matrix) line index (counter +1))

removeFirstElement_c6n8 :: String -> String
removeFirstElement_c6n8 line
	| null line 			= line
	| (head line) == 'X' 	= (head line) : (removeFirstElement_c6n8 (tail line))
	| otherwise				= ('X' : (tail line))

getFirstElement_c6n8 :: String -> Char
getFirstElement_c6n8 line
	| null line 			= '-'
	| (head line) == 'X' 	= (getFirstElement_c6n8 (tail line))
	| otherwise				= (head line)

doesLineHaveValues_c6n8 :: String -> Bool
doesLineHaveValues_c6n8 line
	| null line 			= False
	| (head line) /= 'X'	= True
	| otherwise				= doesLineHaveValues_c6n8 (tail line)


--printStrList :: [String] -> IO ()
--printStrList [] = putStrLn ""
--printStrList (x:xs) = do
 --   putStrLn x
 --   printStrList xs


testBoard = ["1234","567","89","abc","defg"]
testBoard2 = ["wwww","---","--","---","bbbb"]
testBoard3 = ["wwwww","----","---","--" ,"---","----","bbbbb"]


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------END--Parsing Module-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------Static Analyzer----------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



boardScoreWhite_c6n8 :: [String] -> Int
boardScoreWhite_c6n8 board = boardScore_c6n8 board 'w'

boardScoreBlack_c6n8 :: [String] -> Int
boardScoreBlack_c6n8 board = boardScore_c6n8 board 'b'

boardScore_c6n8 :: [String] -> Char -> Int
boardScore_c6n8 board team = countX*(playerCountBonus_c6n8 board team) + adX*(attackDefenceBonus_c6n8 board team) + gsX*(goalStateBonus_c6n8 board team) + (positionBonus_c6n8 board team)

---Count Opponents determins the bonus the state should get by the number of players on the board
playerCountBonus_c6n8 :: [String] -> Char -> Int
playerCountBonus_c6n8 board team
	| team == 'w' 		= (sumPlayers_c6n8 board 'w') - (sumPlayers_c6n8 board 'b')
	| team == 'b'		= (sumPlayers_c6n8 board 'b') - (sumPlayers_c6n8 board 'w')
	| otherwise			= 0

sumPlayers_c6n8 :: [String] -> Char -> Int
sumPlayers_c6n8 board team
	| null board 				= 0
	| null (head board)			= sumPlayers_c6n8 (tail board) team
	| head (head board) == team = (1 + (sumPlayers_c6n8 ((tail (head board)):(tail board)) team))
	| otherwise					= (sumPlayers_c6n8 ((tail (head board)):(tail board)) team)

-- attack defence bonus rates your currnt attacking and defending position as the value of the number of succsusfful atacks you can play - your oponents succussfull attacks

attackDefenceBonus_c6n8 :: [String] -> Char -> Int
attackDefenceBonus_c6n8 board team
	| team == 'w'		= (2*(whiteAttacks_c6n8 board) - (blackAttacks_c6n8 board))
	| team == 'b'		= (2*(blackAttacks_c6n8 board) - (whiteAttacks_c6n8 board))

-- each string denotes a favorable attacking position for a given team, in each of these situations white has the ability to attack, while being defended
-- these states are not given too much value as to supercede actually taking a piece, hower they are good configurations to aim for while trying to win
whiteAttack_c6n8 :: String -> Bool
whiteAttack_c6n8 attackPattern
	| attackPattern == "wwb-" 	= True
	| attackPattern == "-bww" 	= True
	| attackPattern == "-bwX" 	= True
	| attackPattern == "Xwb-" 	= True
	| otherwise 				= False

blackAttack_c6n8 :: String -> Bool
blackAttack_c6n8 attackPattern
	| attackPattern == "bbw-" 	= True
	| attackPattern == "-wbb" 	= True
	| attackPattern == "-wbX" 	= True
	| attackPattern == "Xbw-" 	= True
	| otherwise 				= False

whiteAttackPerLine_c6n8 :: String -> Int
whiteAttackPerLine_c6n8 line 
	| null (tail(tail line))														= 0
	| whiteAttack_c6n8 ((head line): (head (tail line)) : (head(tail(tail line))): []) 	= (1 + (whiteAttackPerLine_c6n8 (tail line)))
	| otherwise																		= (whiteAttackPerLine_c6n8 (tail line))

blackAttacksPerLine_c6n8 :: String -> Int
blackAttacksPerLine_c6n8 line 
	| null (tail(tail line))														= 0
	| blackAttack_c6n8 ((head line): (head (tail line)) : (head(tail(tail line))): []) 	= (1 + (blackAttacksPerLine_c6n8 (tail line)))
	| otherwise																		= (blackAttacksPerLine_c6n8 (tail line))

blackAttacks_c6n8 :: [String] -> Int
blackAttacks_c6n8 board = ((foldr (+) 0 (map blackAttacksPerLine_c6n8 board)) + (foldr (+) 0 (map blackAttacksPerLine_c6n8 (transpose board))))

whiteAttacks_c6n8 :: [String] -> Int
whiteAttacks_c6n8 board = ((foldr (+) 0 (map whiteAttackPerLine_c6n8 board)) + (foldr (+) 0 (map whiteAttackPerLine_c6n8 (transpose board))))

--testBoard 	 = ["1234","567","89","abc","defg"]
--testBoard2 	= ["w-ww","-w-","--","---","bbb-"]
--testBoard3 	= ["wwwww","----","---","--" ,"---","---w","bbbbb"]
--attack 		= ["----","-ww","bb","bbb","----"]

-- goal State is awarded if the team has won the game
goalStateBonus_c6n8 :: [String] -> Char -> Int
goalStateBonus_c6n8 board team
	| (team == 'w') && ((sumPlayers_c6n8 board 'b') == 0) 												= 1
	| (team == 'b') && ((sumPlayers_c6n8 board 'w') == 0) 												= 1
	| (team == 'w') && (endZone_c6n8 (last (parseMatrix_c6n8 board numPlayers)) 'w' (sumPlayers_c6n8 board team) 0)= 1
	| (team == 'b') && (endZone_c6n8 (head (parseMatrix_c6n8 board numPlayers)) 'b' (sumPlayers_c6n8 board team) 0)= 1
	| otherwise																						= 0
	where numPlayers = (((length board) `div` 2)+1)

--end zone is really weird because it actually uses the format of the oska board which was passed originally, simply because it is easier to determine the winning state with that model

endZone_c6n8 :: String ->Char ->Int-> Int-> Bool
endZone_c6n8 line team players count
	| (null line) && (count == players) 	= True
	| (null line) && (count /= players) 	= False 
	| (head line) == team					= endZone_c6n8 (tail line) team players (count+1)
	| otherwise								= endZone_c6n8 (tail line) team players count

--positionBonus

-- Position Bonus nneds to be done for both the white and black pieces

positionBonus_c6n8 :: [String] -> Char -> Int
positionBonus_c6n8 matrix team
	| team == 'w' 		= (sumPositionBonus_c6n8 (parseMatrix_c6n8 matrix (((length matrix) `div` 2)+1)) team 0)		--the funny expression is the number of players
	| team == 'b'		= (sumPositionBonus_c6n8 (reverse(parseMatrix_c6n8 matrix (((length matrix) `div` 2)+1))) team 0)

sumPositionBonus_c6n8 :: [String] -> Char -> Int -> Int
sumPositionBonus_c6n8 oskaBoard team count
	| null oskaBoard 				= 0
	| count == (length oskaBoard) 	= 0
	| otherwise 					= (((count+1)*(sumLine_c6n8 (head oskaBoard) team )) + (sumPositionBonus_c6n8 (tail oskaBoard) team (count +1)))

sumLine_c6n8 :: String -> Char -> Int
sumLine_c6n8 line team
	| null line 	      	= 0
	| (head line) == team   = (1 + (sumLine_c6n8 (tail line) team))
	| otherwise				= sumLine_c6n8 (tail line) team  
-------end POsition bonus
o1 = [
	"XXX-XX", 
	"XX--XX", 
	"X---w-", 
	"b----X", 
	"XX-wXX", 
	"XXwXXX"]



countX :: Int 
countX 	= 100
adX ::Int
adX		= 25
gsX :: Int
gsX		= 1000

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------END-Static Analyzer-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
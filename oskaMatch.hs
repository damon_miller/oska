
-- Import your oska programs so that they may do battle
import Oska
-- import player1
-- import player2

--		The call is
-- 		match program1 program2 players depthOfSearch
play = match oska_c6n8 oska_c6n8 4 5

match :: ([String] -> Char -> Int -> [String]) -> ([String] -> Char -> Int -> [String]) -> Int -> Int -> IO ()
match player1 player2 pieces deep = printAll(playMatch player1 player2 (genBoard pieces) True deep)

playMatch :: ([String] -> Char -> Int -> [String]) -> ([String] -> Char -> Int -> [String]) -> [String] -> Bool-> Int -> [[String]]
playMatch player1 player2 board turn deep
	| goalState board 		= board : []
	| turn					= board : (playMatch player1 player2 (player1 board 'w' deep) False deep)
	| otherwise				= board : (playMatch player1 player2 (player2 board 'b' deep) True deep)

genBoard :: Int -> [String]
genBoard players = (genHalfBoard 'w' players) ++ (tail(reverse (genHalfBoard 'b' players)))

genHalfBoard :: Char -> Int -> [String]
genHalfBoard team players
	| players < 2		= []
	| otherwise			= (makeLine team players) : (genHalfBoard '-' (players -1))

makeLine :: Char -> Int -> String
makeLine team count
	| count == 0	= []
	| otherwise		= team : (makeLine team (count -1))

goalState :: [String] -> Bool
goalState board
	| ((sumPlayers board 'b') == 0) 							= True
	| ((sumPlayers board 'w') == 0) 							= True
	| (endZone (last board) 'w' (sumPlayers board 'w') 0)		= True
	| (endZone (head board) 'b' (sumPlayers board 'b') 0)		= True
	| otherwise													= False

sumPlayers :: [String] -> Char -> Int
sumPlayers board team
	| null board 				= 0
	| null (head board)			= sumPlayers (tail board) team
	| head (head board) == team = (1 + (sumPlayers ((tail (head board)):(tail board)) team))
	| otherwise					= (sumPlayers ((tail (head board)):(tail board)) team)

endZone :: String ->Char ->Int-> Int-> Bool
endZone line team players count
	| (null line) && (count == players) 	= True
	| (null line) && (count /= players) 	= False 
	| (head line) == team					= endZone (tail line) team players (count+1)
	| otherwise								= endZone (tail line) team players count

printAll :: [[String]] -> IO()
printAll game
	| null(tail game)	=printer (head game)
	| otherwise			= do
							printer (head game)
							printAll (tail game)
	
printer :: [String] -> IO()
printer state = display state (length state)

display :: [String] -> Int -> IO()
display [] n = putStrLn " "
display (x:xs) n = do
	putStr[' ' | i<-[1..n - (length x)]]
	putStrLn (concat [c:' ':[] | c <- x])
	display xs n
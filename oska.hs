module Oska where

import Data.List -- transpose, maximumBy, minimumBy
import Data.Function -- `on`
import V9C9 --(parseOskaBoard_c6n8),(parseMatrix_c6n8),(boardScoreWhite_c6n8),(boardScoreBlack_c6n8)

oska_c6n8 :: [String] -> Char -> Int -> [String]
oska_c6n8 board team depth
	| team == 'w'   = parseMatrix_c6n8 (head (tail(fst(minimaxForWhite (parseOskaBoard_c6n8 board) depth (boardScoreWhite_c6n8))))) (length(head board))
	| team == 'b'   = parseMatrix_c6n8 (head (tail(fst(minimaxForBlack (parseOskaBoard_c6n8 board) depth (boardScoreBlack_c6n8))))) (length(head board))
	| otherwise		= board

test1 = printStrList(oska_c6n8 ["www-","w--","--","---","bbbb"] 'b' 5);

test2 = oska_c6n8 board2 'b' 3
test3 = oska_c6n8 test2 'w' 3

board2 = ["www--","w---","wb-","--","---","--w-","bbbb-"]

-- // utility functions

pushFrontN_Elements :: a -> [a] -> Int -> [a]
pushFrontN_Elements e list n
	| n == 0 		= list
	| otherwise 	= e:(pushFrontN_Elements e list (n-1) )

generateN_Elements :: a -> Int -> [a]
generateN_Elements e n = pushFrontN_Elements e [] n

replaceElementAtPos :: Int -> [a] -> a -> [a]
replaceElementAtPos i list e = replaceElement (splitAt i list) e
	where 	replaceElement ([],[]) e 		= [e];
			replaceElement (list1,[]) e 	= list1 ++ [e];
			replaceElement ([],_:ys) e 		= e:ys;
		 	replaceElement (list1,_:ys) e 	= list1 ++ (e:ys)

-- testing
reap1 = replaceElementAtPos 0 [] 'x'
reap2 = replaceElementAtPos 0 "ab" 'x'
reap3 = replaceElementAtPos 1 "ab" 'x'
reap4 = replaceElementAtPos 2 "ab" 'x'


replaceElementAtPosWithSpacing:: Int -> [a] -> a -> a -> [a]
replaceElementAtPosWithSpacing i list e sp
	| i >= len 		= list ++ (generateN_Elements sp (i-len) ) ++ [e]
	| otherwise 	= replaceElementAtPos i list e
	where len = length list

-- testing
reapws1 = replaceElementAtPosWithSpacing 1 [] 'x' '-'
reapws2 = replaceElementAtPosWithSpacing 2 "ab" 'x' '-'
reapws3 = replaceElementAtPosWithSpacing 3 "ab" 'x' '-'
reapws4 = replaceElementAtPosWithSpacing 4 "ab" 'x' '-'


replaceElementAtPosPosWithSpacing :: [[a]] -> Int -> Int -> a -> a -> [[a]]
replaceElementAtPosPosWithSpacing listOfLists i1 i2 e sp = 
	replaceElementAtPosWithSpacing
		i1
		listOfLists
		(replaceElementAtPosWithSpacing i2 curList e sp)
		[]
	where curList =
		if (i1 < (length listOfLists))
			then (!!) listOfLists i1
			else []

-- testing
reappws1 = replaceElementAtPosPosWithSpacing [] 0 0 'x' '-'
reappws2 = replaceElementAtPosPosWithSpacing [] 1 0 'x' '-'
reappws3 = replaceElementAtPosPosWithSpacing [] 0 1 'x' '-'
reappws4 = replaceElementAtPosPosWithSpacing [] 1 1 'x' '-'
reappws5 = replaceElementAtPosPosWithSpacing ["ab"] 0 0 'x' '-'
reappws6 = replaceElementAtPosPosWithSpacing ["ab"] 3 0 'x' '-'
reappws7 = replaceElementAtPosPosWithSpacing ["ab"] 0 3 'x' '-'
reappws8 = replaceElementAtPosPosWithSpacing ["ab"] 3 3 'x' '-'



-- // board functions

wall = 'X'
type Matrix = [String]
type Oska = [String]


matrixToOska :: Matrix -> Int -> Int -> Oska -> Oska
matrixToOska matrix rpos cpos oska
	| null matrix = []
	| (rpos < m && cpos < m) = 
		if ((getMatrixElem matrix rpos cpos) /= wall )
			then matrixToOska matrix rpos (cpos+1) updatedOska
			else matrixToOska matrix rpos (cpos+1) oska

	| cpos == m 	= matrixToOska matrix (rpos+1) 0 oska
	-- rpos == m
	| otherwise 	= oska

	where 	m 				= length matrix;
			k 				= m - 1;
			inUpperHalf 	= rpos <= ((m-1) - cpos);
			n 				= (m + 2) `div` 2;
			mid_vpos 		= (k - 1) `div` 2;

			hpos
				| inUpperHalf 	= (n-1) - rpos
				| otherwise 	= cpos - mid_vpos;
			
			vpos
				| inUpperHalf 	= cpos - hpos
				| otherwise 	= rpos + hpos - 1;
			
			updatedOska = 
				(replaceElementAtPosPosWithSpacing
					oska
					vpos
					hpos
					(getMatrixElem matrix rpos cpos)
					wall)

matrixToOska_main :: Matrix -> Oska
matrixToOska_main matrix = matrixToOska matrix 0 0 []

getMatrixElem :: Matrix -> Int -> Int -> Char
--getMatrixElem matrix rpos cpos 	= ((!!) ((!!) matrix rpos) cpos)
getMatrixElem matrix rpos cpos 	= ((matrix !! rpos) !! cpos)

-- testing
mto1 = matrixToOska_main oskaMatrixAlphabet
mto2 = matrixToOska_main oskaMatrixStart
mto3 = matrixToOska_main []

oskaMatrixAlphabet = [
	"XXXdXX", 
	"XXcgXX", 
	"Xbfilp", 
	"aehkoX", 
	"XXjnXX", 
	"XXmXXX"]

oskaMatrixStart = [
	"XXXwXX", 
	"XXw-XX", 
	"Xw---b", 
	"w---bX", 
	"XX-bXX", 
	"XXbXXX"]

oskaMatrixPrac1 = [
	"XXXwXX", 
	"XXw-XX", 
	"Xw---b", 
	"w--wbX", 
	"XX-bXX", 
	"XXbXXX"]

oskaStart = ["wwww","---","--","---","bbbb"]



-- // move operators
{- adapted from Kurt Eiselt's PegPuzzle.hs -}

replaceSegment oldList pos segment
   | pos == 0  = segment ++ drop (length segment) oldList
   | otherwise = 
        (head oldList):
        (replaceSegment (tail oldList) (pos - 1) segment)

-- oska white player
generateNewWhiteRightSlides currSeg = 
	generateNew currSeg 0 "w-" "-w"

generateNewWhiteRightJumps currSeg =
	generateNew currSeg 0 "wb-" "--w"

generateNewWhiteLeftSlides currSeg = 
	reverseEach (generateNew (reverse currSeg) 0 "w-" "-w")
	
generateNewWhiteLeftJumps currSeg =
	reverseEach (generateNew (reverse currSeg) 0 "wb-" "--w")

--whiteRightLeftOperators = [
--	generateNewWhiteRightSlides,
--	generateNewWhiteRightJumps,
--	generateNewWhiteLeftSlides,
--	generateNewWhiteLeftJumps
--	]

-- oska black player
generateNewBlackRightSlides currSeg = 
	generateNew currSeg 0 "b-" "-b"

generateNewBlackRightJumps currSeg =
	generateNew currSeg 0 "bw-" "--b"

generateNewBlackLeftSlides currSeg = 
	reverseEach (generateNew (reverse currSeg) 0 "b-" "-b")
	
generateNewBlackLeftJumps currSeg =
	reverseEach (generateNew (reverse currSeg) 0 "bw-" "--b")

--blackRightLeftOperators = [
--	generateNewBlackRightSlides,
--	generateNewBlackRightJumps,
--	generateNewBlackLeftSlides,
--	generateNewBlackLeftJumps
--	]


generateNew :: String -> Int -> String -> String -> [String]
generateNew currState pos oldSegment newSegment
   | pos + (length oldSegment) > length currState    = []
   | segmentEqual currState pos oldSegment           =
        (replaceSegment currState pos newSegment):
        (generateNew currState (pos + 1) oldSegment newSegment)
   | otherwise                                       =
        (generateNew currState (pos + 1) oldSegment newSegment)

segmentEqual currState pos oldSegment = 
   (oldSegment == take (length oldSegment) (drop pos currState))

reverseEach :: [[a]] -> [[a]]
reverseEach listOfLists = map reverse listOfLists



-- // move generators

-- generates all possible new states based on the type of operator (right,down,left,up)
newStateGenerator :: Matrix -> Int -> (String -> [String]) -> [Matrix]
newStateGenerator currState pos movesGenerator
	| null currState 	= []
	| pos == last 		= generatedStates
	| otherwise 		= 
		generatedStates ++ (newStateGenerator currState (pos + 1) movesGenerator)
	where 	generatedStates = 
				map 
					(replaceElementAtPos pos currState)
					(movesGenerator (currState !! pos) );
			last = -1 + (length currState)


--- WHITE move generators
-- right
generateNewStatesFromNewWhiteRightSlides currState = 
	newStateGenerator currState 0 generateNewWhiteRightSlides

generateNewStatesFromNewWhiteRightJumps currState = 
	newStateGenerator currState 0 generateNewWhiteRightJumps

-- left
generateNewStatesFromNewWhiteLeftSlides currState = 
	newStateGenerator currState 0 generateNewWhiteLeftSlides 

generateNewStatesFromNewWhiteLeftJumps currState = 
	newStateGenerator currState 0 generateNewWhiteLeftJumps 

-- right, left
--generateNewStatesFromWhiteRightLeftOperators currState = 
--	concatMap (newStateGenerator currState 0) whiteRightLeftOperators


-- down
generateNewStatesFromNewWhiteDownSlides currState = 
	map 
		transpose 
		(generateNewStatesFromNewWhiteRightSlides (transpose currState) )

generateNewStatesFromNewWhiteDownJumps currState = 
	map 
		transpose 
		(generateNewStatesFromNewWhiteRightJumps (transpose currState) )

-- up
generateNewStatesFromNewWhiteUpSlides currState = 
	map 
		transpose 
		(generateNewStatesFromNewWhiteLeftSlides (transpose currState) )

generateNewStatesFromNewWhiteUpJumps currState = 
	map 
		transpose 
		(generateNewStatesFromNewWhiteLeftJumps (transpose currState) )

-- down, up
--generateNewStatesFromWhiteDownUpOperators currState = 
--	map 
--		transpose
--		(generateNewStatesFromWhiteRightLeftOperators (transpose currState) )


--- BLACK move generators
-- right
generateNewStatesFromNewBlackRightSlides currState = 
	newStateGenerator currState 0 generateNewBlackRightSlides

generateNewStatesFromNewBlackRightJumps currState = 
	newStateGenerator currState 0 generateNewBlackRightJumps

-- left
generateNewStatesFromNewBlackLeftSlides currState = 
	newStateGenerator currState 0 generateNewBlackLeftSlides

generateNewStatesFromNewBlackLeftJumps currState = 
	newStateGenerator currState 0 generateNewBlackLeftJumps

-- right, left
--generateNewStatesFromBlackRightLeftOperators currState = 
--	concatMap (newStateGenerator currState 0) blackRightLeftOperators


-- down
generateNewStatesFromNewBlackDownSlides currState = 
	map 
		transpose 
		(generateNewStatesFromNewBlackRightSlides (transpose currState) )

generateNewStatesFromNewBlackDownJumps currState = 
	map 
		transpose 
		(generateNewStatesFromNewBlackRightJumps (transpose currState) )

-- up
generateNewStatesFromNewBlackUpSlides currState = 
	map 
		transpose 
		(generateNewStatesFromNewBlackLeftSlides (transpose currState) )

generateNewStatesFromNewBlackUpJumps currState = 
	map 
		transpose 
		(generateNewStatesFromNewBlackLeftJumps (transpose currState) )

-- down, up
--generateNewStatesFromBlackDownUpOperators currState = 
--	map 
--		transpose
--		(generateNewStatesFromBlackRightLeftOperators (transpose currState) )


{- adapted from Kurt Eiselt's PegPuzzle.hs -}

generateNewStatesFromWhite :: Matrix -> [Matrix]
generateNewStatesFromWhite currState = 
	concat [
		---- right
		generateNewStatesFromNewWhiteRightSlides currState, 
		generateNewStatesFromNewWhiteRightJumps currState,

		-- left
		generateNewStatesFromNewWhiteLeftSlides currState, 
		generateNewStatesFromNewWhiteLeftJumps currState, 

		---down
		generateNewStatesFromNewWhiteDownSlides currState, 
		generateNewStatesFromNewWhiteDownJumps currState, 

		-- up
		generateNewStatesFromNewWhiteUpSlides currState, 
		generateNewStatesFromNewWhiteUpJumps currState


		--generateNewStatesFromWhiteRightLeftOperators currState,
		--generateNewStatesFromWhiteDownUpOperators currState
		]

generateNewStatesFromBlack :: Matrix -> [Matrix]
generateNewStatesFromBlack currState = 
	concat [
		-- right
		generateNewStatesFromNewBlackRightSlides currState, 
		generateNewStatesFromNewBlackRightJumps currState,

		-- left
		generateNewStatesFromNewBlackLeftSlides currState, 
		generateNewStatesFromNewBlackLeftJumps currState, 

		---down
		generateNewStatesFromNewBlackDownSlides currState, 
		generateNewStatesFromNewBlackDownJumps currState, 

		-- up
		generateNewStatesFromNewBlackUpSlides currState, 
		generateNewStatesFromNewBlackUpJumps currState


		--generateNewStatesFromBlackRightLeftOperators currState,
		--generateNewStatesFromBlackDownUpOperators currState
		]



-- // evaluator functions

countBlack segment = length [x | x <- segment, x == 'b']
countWhite segment = length [x | x <- segment, x == 'w']

evaluatorForWhite node = 
	(sum (map countWhite node)) - (sum (map countBlack node))
evaluatorForBlack node = -1 * (evaluatorForWhite node)

evaluator1 currState = 1;


-- // minimax mechanism

type Evaluator = Matrix -> Int
type Generator = Matrix -> [Matrix]


{- 	adapted from
		http://en.wikipedia.org/wiki/Minimax -> Pseudocode:
			function minimax(node, depth, maximizingPlayer)
			    if depth = 0 or node is a terminal node
			        return the heuristic value of node
			    if maximizingPlayer
			        bestValue := -∞
			        for each child of node
			            val := minimax(child, depth - 1, FALSE))
			            bestValue := max(bestValue, val);
			        return bestValue
			    else
			        bestValue := +∞
			        for each child of node
			            val := minimax(child, depth - 1, TRUE))
			            bestValue := min(bestValue, val);
			        return bestValue

			(* Initial call for maximizing player *)
			minimax(origin, depth, TRUE)
-}
minimax :: 
	Matrix -> Int -> Bool -> 
	Generator -> Generator -> Evaluator -> [Matrix] -> 
	([Matrix], Int)
minimax 
	parentNode depth isMAX 
	childGeneratorOfMAX childGeneratorOfMIN evaluator path
	| depth == 0 		= 
		(reverse updatedPath, evaluator parentNode)
	
	| isMAX 			= 
		if (playerCanMakeMove childrenOfMAX)
			then 
				maximumBy 
					(compare `on` snd) 
					(map minimaxForChild childrenOfMAX)
			else minimaxPlayerLosesTurn
	-- isMIN
	| otherwise 		= 
		if (playerCanMakeMove childrenOfMIN)
			then 
				minimumBy 
					(compare `on` snd) 
					(map minimaxForChild childrenOfMIN)
			else minimaxPlayerLosesTurn

	where 	updatedPath = parentNode:path;

			playerCanMakeMove children = not (null children);			
			
			childrenOfMAX = childGeneratorOfMAX parentNode;
 			childrenOfMIN = childGeneratorOfMIN parentNode;
 			
 			minimaxForChild childNode = 
				minimax 
					childNode 
					(depth - 1) 
					(not isMAX)
					childGeneratorOfMAX
					childGeneratorOfMIN
					evaluator
					updatedPath;
					
			minimaxPlayerLosesTurn = 
				minimax 
					parentNode 
					(depth - 1) 
					(not isMAX) 
					childGeneratorOfMAX 
					childGeneratorOfMIN 
					evaluator 
					updatedPath



minimaxForWhite :: Matrix -> Int -> Evaluator -> ([Matrix], Int)
minimaxForWhite node depth evaluatorForWhite = 
	minimax node depth True generateNewStatesFromWhite generateNewStatesFromBlack evaluatorForWhite []

minimaxForBlack :: Matrix -> Int -> Evaluator -> ([Matrix], Int)
minimaxForBlack node depth evaluatorForBlack = 
	minimax node depth True generateNewStatesFromBlack generateNewStatesFromWhite evaluatorForBlack []

-- testing
testWhite = minimaxForWhite oskaMatrixTest 7 evaluatorForWhite
testBlack = minimaxForBlack oskaMatrixTest 5 evaluatorForBlack
whiteNoMoves = minimaxForWhite oskaMatrixWhiteNoMove 4 evaluatorForWhite

testWhitePrint = printStrMatrix (fst testWhite)
testBlackPrint = printStrMatrix (fst testBlack)
whiteNoMovesPrint = printStrMatrix (fst whiteNoMoves)

oskaMatrixTest = [
	"XXX-XX", 
	"XX--XX", 
	"X--w--", 
	"w-bb-X", 
	"XX--XX", 
	"XX-XXX"]

oskaMatrixWhiteNoMove = [
	 "XXX-XX", 
	 "XX--XX", 
	 "X-----", 
	 "wbb--X", 
	 "XX--XX", 
	 "XX-XXX"]

--bestNextMove depth currNode nodeGenerator depth nextNode = 
--	| depth = 0 
--	| otherwise = 
--		if evalu()



-- // printing functions

--- Printing assignment 3 nicely in the console
--- Author: Rodrigo Alves


-- Calls the oska function (same params), but prints the next move nicely.

-- Call example: oska_print_yourID ["www-","-w-","--","b--","-bbb"] 'b' 4
{-
oska_print_yourID :: [String] -> Char -> Int -> IO ()

oska_print_yourID board side depth = print_yourID (oska_yourID board side depth)
-}
 


{----------------------------------------------------------------

------------------------- PRINT FUNCTIONS ----------------------

-----------------------------------------------------------------}

 

-- @param board: the board that will be printed. Prints in the console.

-- Call example 1: print_yourID ["ww-","-w","bbb"]

-- Call example 2: print_yourID ["w-w-w","--w-","w--","--","---","--bb","bb-b-"]

-- print_yourID ["wwwwww","-----","----", "-T-", "--", "---","----","-----","bbbbbb"]

print_yourID :: [String] -> IO ()

print_yourID board = print_board_yourID board 1 (length board) 0 (-1)

       

-- Print helper to print the board on console. Magic happens here.

print_board_yourID :: [String] -> Int -> Int -> Int -> Int -> IO ()

print_board_yourID [] _ _ previousLength previousIndent =

        print_line_yourID previousLength previousIndent (even previousIndent)

print_board_yourID (x:xs) currentRow totalRows previousLength previousIndent = do

        print_line_yourID (max (length x) previousLength) lineIndent (even lineIndent)

        print_row_yourID x indent

        print_board_yourID xs (currentRow + 1) totalRows (length x) indent

        where indent = next_indent_yourID previousIndent totalRows currentRow

              lineIndent = max indent previousIndent

 

-- @return the next indent based on the idea that the indent is getting

-- bigger until half of the board. Then, it starts to shrink.

next_indent_yourID :: Int -> Int -> Int -> Int

next_indent_yourID previousIndent totalRows currentRow

        | currentRow > ((div (totalRows+1) 2))        = previousIndent - 1

        | otherwise                             = previousIndent + 1

       

-- @param size: the size of the row of lines (about 4 times that)

-- @param indent: the space before printing the row of lines

-- @param evenIndent: helper to determine if an extra '-' or space is required

-- Call example: print_line_yourID 3 2 True -> prints "  -------------"

print_line_yourID :: Int -> Int -> Bool -> IO ()

print_line_yourID 0 indent evenIndent

        | evenIndent = putStrLn "-"

        | otherwise = putStrLn ""

print_line_yourID size 0 evenIndent = do

        putStr "----"

        print_line_yourID (size-1) 0 evenIndent

print_line_yourID size 1 False = do

        putStr "-"

        print_line_yourID size 0 False

print_line_yourID size 1 True = print_line_yourID size 0 True

print_line_yourID size indent evenIndent = do

        putStr "  "

        print_line_yourID size (indent - 1) evenIndent

       

-- @param str: a string that will be printed in the console

-- @param indent: how much space should be printed before printing str.

-- Call example 1: print_row_yourID "wwww" 3 -> prints "      | w | w | w | w |"

print_row_yourID :: String -> Int -> IO ()

print_row_yourID [] _ = putStrLn "|"

print_row_yourID (x:xs) 0 = do

        print_piece_yourID x

        print_row_yourID xs 0 

print_row_yourID str indent = do

        putStr "  "

        print_row_yourID str (indent - 1)

 

-- Helper of the print function that prints a small portion of a row      

print_piece_yourID :: Char -> IO ()

print_piece_yourID 'w' = putStr "| w "

print_piece_yourID 'b' = putStr "| b "

print_piece_yourID '-' = putStr "|   "

print_piece_yourID  c  = putStr "| ? "




--- Printing assignment 3 nicely in the console
--- Author: Rodrigo Alves

-- Solve rush_hour, but outputs nicely in the console
--rush_print :: [String] -> IO ()
--rush_print start = printStrMatrix (rush_hour start)

-- Get a list of lists of strings and output them nicely.
printStrMatrix :: [[String]] -> IO ()
printStrMatrix [] = printStrList []
printStrMatrix (x:xs) = do
    printStrList x
    printStrMatrix xs

-- Print nicely a list of strings.
-- Eg: printStrList ["aabb", "ccdd", "eeff"] prints in the console:
-- aabb
-- ccdd
-- eeff
printStrList :: [String] -> IO ()
printStrList [] = putStrLn ""
printStrList (x:xs) = do
    putStrLn x
    printStrList xs

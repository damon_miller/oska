testBoard = ["wwww","---","--","---","bbbb"]

makeMatrix :: [String] -> [String]
makeMatrix input = genBoard (2*(length (input!!0)) -2) (2*(length (input!!0)) -2)

genBoard :: Int -> Int -> [String]
genBoard size index
	| index == 0		= []
	| otherwise			= (xLine size) : (genBoard size (index - 1))

xLine :: Int -> String
xLine index
	| index == 0		=[]
	| otherwise			= 'x': (xLine (index -1)) 

populateMatrix :: [String] -> [String] -> Int -> Int-> [String]
populateMatrix input matrix listNumber indexOfElement
	| listNumber == (length matrix)			= matrix
	| otherwise								= populateMatrix input (addLine matrix (input !! listNumber) (length (input !!0)) 0 listNumber) (listNumber+1) (indexOfElement + length(input !! listNumber))

addLine :: [String] -> String -> Int -> Int -> Int -> Int->[String]
addLine matrix line numPlayers counter listNumber indexOfElement
	| counter == listNumber  								= matrix
	| (indexOfElement < ((numPlayers*(numPlayers + 1))/2)) 	= addLine (appendLine matrix (addElement (matrix !! (numPlayers-counter -1)) (line !! counter) numPlayers counter listNumber indexOfElement (length matrix) 0) listNumber 0) line numPlayers (counter + 1) listNumber (indexOfElement+1)
	| otherwise												= addLine (appendLine matrix (addElement (matrix !! (listNumber - counter + 1)) (line !! counter) numPlayers counter listNumber indexOfElement (length matrix) 0) listNumber 0) line numPlayers (counter + 1) listNumber (indexOfElement+1)

-- at the moment this does not work because the line is not injected back into the matrix in the correct location I will need another helper function

appendLine :: [String] -> String -> Int -> Int -> [String]
appendLine matrix matrixLine linenumber count
	| count == linenumber		= matrixLine : (tail matrix)
	| otherwise					= (head matrix) : (appendLine (tail matrix) matrixLine linenumber (count+1))	

addElement :: String -> Char -> Int -> Int -> Int -> Int -> Int-> Int-> String
addElement matrixLine character numPlayers listPosition listNumber indexOfElement lineLength count
	| (indexOfElement <  ((fromIntegral(numPlayers*(numPlayers + 1))/2))) && (count==(listPosition + listNumber)) 											= character : (tail matrixLine) 
	| (indexOfElement >= ((fromIntegral(numPlayers*(numPlayers + 1))/2))) && (count==(lineLength - numPlayers + listPosition -1))			= character : (tail matrixLine)
	| otherwise																																= (head matrixLine) : (addElement (tail matrixLine) character numPlayers listPosition listNumber indexOfElement lineLength count)